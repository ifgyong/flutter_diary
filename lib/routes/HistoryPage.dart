import 'package:flutter/cupertino.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterbook/comment/Glo.dart';
import 'package:flutterbook/comment/config/LocalConfig.dart';
import 'package:flutterbook/routes/easy_hub.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutterbook/main.dart';
import 'AddArticle.dart';
import 'SecondPage.dart';

class HistoryPage extends StatefulWidget {
  final String title;
  final bool isToday;
  HistoryPage({Key key, this.title, this.isToday}) : super(key: key);
  _History createState() => _History();
}

class _History extends State<HistoryPage> {
  List _data;
  int itemCount = 0;
  int _page = 1;
  JPush _jPush = new JPush();
  EasyRefreshController _controller;
  ScrollController _scrollController;
  String _nodataString = '';

  @override
  Future<void> initState() {
    super.initState();
    //为什么这样子就 可以 否则会报错
    Future.delayed(Duration.zero, () {
      this.update(1); //刷新列表
    });
    _controller = EasyRefreshController();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
    EasyHub.dismiddAll();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('did changed');
  }

  void update(int pp) async {
    if (context != null) {
      EasyHub.show(context, 'lading');
    }
    print('context:$context');
    try {
      String url = Config.url() + '/api/list?page=$pp';
      if (widget.isToday != null && widget.isToday) {
        url = Config.url() + '/api/today?page=$pp';
      }
      Response response = await Dio().get(url);

      List data = response.data['data']['data'];
      EasyHub.dismiddAll();
      if (getItemCount(data) > 0) {
        setState(() {
          _nodataString = '';
          if (pp > 1) {
            _data += response.data['data']['data'];
          } else {
            _data = response.data['data']['data'];
          }
          itemCount = _data.length;
          _page += 1;
        });
      } else {
        setState(() {
          _nodataString = '暂无数据哦';
        });
      }

      _controller.finishRefresh(success: true, noMore: false);
      _controller.finishLoad(success: true, noMore: false);

//      print('异步回调：page:$pp,$_page' + _data.toString());
    } catch (e) {
      print('错误：' + e);
    }
  }

  int getItemCount(List list) {
    if (list == null) {
      return 0;
    }
    return list.length;
  }

//列表
  ListView getListView() {
    return ListView.builder(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => AddArticle(
                        title: '添加提醒',
                        titleValue: _data[index]['title'],
                        textValue: _data[index]['content'],
                        id: _data[index]['id'].toString(),
                      )));
            },
            child: Container(
              margin: EdgeInsets.only(top: 5, bottom: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Container(
                  color: Global.defaultTextBgColor,
                  width: 100,
                  height: 103,
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 15, right: 15, top: 8, bottom: 8),
                    child: Column(
//                      padding: EdgeInsets.only(left: 15, right: 15),

                      children: <Widget>[
                        Flex(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Expanded(
                              flex: 5,
//                              alignment: Alignment.topLeft,
//                              padding: EdgeInsets.only(
//                                  left: 15, right: 15, top: 8, bottom: 8),
                              child: Text(
                                _data[index]['title'],
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              ),
                            ),
                            Text(
                              _data[index]['time'] == null
                                  ? ''
                                  : _data[index]['time'],
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black38),
                            )
                          ],
                        ),
                        Container(
//                          padding: EdgeInsets.only(left: 15, right: 15),
                          alignment: Alignment.topLeft,
                          child: Text(
                            _data[index]['content'],
                            style:
                                TextStyle(fontSize: 13, color: Colors.black38),
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
      itemCount: itemCount,
    );
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Stack(
            children: <Widget>[
              EasyRefresh(
                child: getListView(),
                onLoad: () async {
                  print('onload');
                  update(_page);
                },
                onRefresh: () async {
                  print('onRefresh');
                  _page = 1;
                  update(_page);
                },
                controller: _controller,
                scrollController: _scrollController,
                enableControlFinishLoad: true,
                enableControlFinishRefresh: true,
              ),
              Positioned(
                top: _size.height / 2 - 15,
                left: 20,
                width: _size.width - 40,
                height: 30,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(_nodataString),
                ),
              )
            ],
          ),
        )
//      Container(
//        color: Global.defaultBgColor,
//        alignment: Alignment.topLeft,
//        child: EasyRefresh(
//          child: getListView(),
//          onLoad: () async {
//            print('onload');
//            update(_page);
//          },
//          onRefresh: () async {
//            print('onRefresh');
//            _page = 1;
//            update(_page);
//          },
//          controller: _controller,
//          scrollController: _scrollController,
//          enableControlFinishLoad: true,
//          enableControlFinishRefresh: true,
//        ),
//      ),
        );
  }
}
