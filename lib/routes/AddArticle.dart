import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterbook/comment/DataBaseComment.dart';
import 'package:flutterbook/comment/Glo.dart';
import 'package:flutterbook/comment/config/LocalConfig.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;
import 'dart:io';

class AddArticle extends StatefulWidget {
  final String title;
  final String textValue;
  final String titleValue;
  final String id;
  AddArticle({Key key, this.title, this.textValue, this.titleValue, this.id})
      : super(key: key);
  _AddArticle createState() => _AddArticle();
}

class _AddArticle extends State<AddArticle> {
  Scaffold _scaffold;

  String _textValue = '';
  String _titleValue = '';
  String _id;
  TextEditingController _textEditingController;
  TextEditingController _contentEditingController;

  String iconPath = 'images/clock_dark.png';
  @override
  void initState() {
    super.initState();
    _id = widget.id;

    _textEditingController = TextEditingController();
    _contentEditingController = TextEditingController();
    _titleValue = widget.titleValue;
    _textValue = widget.textValue;
    _id = widget.id;

    if (_titleValue != null && _textValue != null) {
      _textEditingController.text = _titleValue;
      _contentEditingController.text = _textValue;
      print('title:' + _titleValue + 'text:' + _textValue);
    }
    getInfoFromServer(_id);
  }

  void getInfoFromServer(String id) async {
    if (id == null || id.length == 0) return;

    Response response = await Dio().get(Config.url() + '/api/info?id=$id');
    print('getinfo:' + response.data.toString());
    if (response.data['status'] == 'ok') {
      setState(() {
        _titleValue = response.data['data']['title'];
        _textValue = response.data['data']['content'];
        _contentEditingController.text = _textValue;
        _textEditingController.text = _titleValue;
        clockStr = response.data['data']['time'];
        if (clockStr != null) {
          iconPath = 'images/clock_light.png';
          _clock = DateTime.tryParse(clockStr);
        } else {
          clockStr = ' ';
        }
      });
    }
  }

  /*本地推送*/
  void push(String title, String content, String secondTitle,
      {DateTime firdate}) {
    if (firdate == null) {
      firdate = DateTime.now().add(Duration(seconds: 1));
    }
//    firdate = DateTime.now().add(Duration(seconds: 1));
    print(firdate.toString());
    var nf = LocalNotification(
        id: 123,
        title: title,
        content: content,
        fireTime: firdate,
        subtitle: secondTitle,
        badge: 1,
        extra: {'key': '我是附带参数'});
    new JPush().sendLocalNotification(nf);
  }

  void _showError(BuildContext context, String title, String content) {
    String error = '';
    if (title.length == 0) {
      error = '标题必须填写哦';
    } else if (content.length == 0) {
      error = '内容必填哦';
    }

    if (error.length > 0) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(error),
        action: SnackBarAction(
          label: '知道了',
          onPressed: () {},
        ),
      ));
    } else {
      Future ret = Dio().post(Config.url() + '/api/add', queryParameters: {
        'title': title,
        'content': content,
        'date': clockStr,
        'id': _id
      });
      ret.then((response) {
        if (response.data['status'] == 'ok') {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('保存成功'),
            action: SnackBarAction(
              label: '',
              onPressed: () {},
            ),
          ));
          //推送本地的
          push(_titleValue, content, content, firdate: _clock);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.pop(context);
          });
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(response.data['msg']),
            action: SnackBarAction(
              label: '知道了',
              onPressed: () {},
            ),
          ));
        }
      });

      return;
    }
  }

  Container textFieldWidget(BuildContext context) {
    return Container(
      color: Global.defaultTextBgColor,
      padding: EdgeInsets.only(left: 15, right: 15),
      child: Column(
        children: <Widget>[
          TextField(
            maxLines: 1,
//            maxLength: 20,
            maxLengthEnforced: false,
            controller: _textEditingController,
            toolbarOptions: ToolbarOptions(
                cut: true, copy: true, selectAll: true, paste: true),
            style: TextStyle(fontSize: 20),
            onEditingComplete: () {
              print('编辑完成');
            },
            onChanged: (text) {
              _titleValue = text;
            },
            decoration: InputDecoration(labelText: '标题', hintText: '请填写标题~'),
          ),
          TextField(
            maxLines: 5,
            maxLength: 199,
            controller: _contentEditingController,
            toolbarOptions: ToolbarOptions(
                cut: true, copy: true, selectAll: true, paste: true),
            style: TextStyle(fontSize: 20),
            onEditingComplete: () {
              print('编辑完成');
            },
            onChanged: (text) {
              print(text);
              _textValue = text;
            },
            decoration: InputDecoration(
                hintText: '为了更好地记录请完善内容哦~', alignLabelWithHint: true),
          )
        ],
      ),
    );
  }

  DateTime _clock;
  String clockStr = ' ';
  showDate(BuildContext context) async {
    DateTime nowChagne;
    var datepicker = await CupertinoDatePicker(
      initialDateTime: DateTime.now(),
      use24hFormat: true,
      onDateTimeChanged: (day) {
        nowChagne = day;
      },
    );
    _clock = null;
    return showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return Container(
            height: 248,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: FlatButton(
                        child: Text('取消'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          setState(() {
                            iconPath = 'images/clock_dark.png';
                            timeStr();
                          });
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: FlatButton(
                        child: Text('确定'),
                        onPressed: () {
                          print(nowChagne);
                          _clock = nowChagne;
                          setState(() {
                            iconPath = 'images/clock_light.png';
                            timeStr();
                          });
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  child: datepicker,
                  height: 200,
                ),
              ],
            ),
          );
        });
  }

  void updateIconPath() {}

  @override
  Widget build(BuildContext context) {
    _scaffold = Scaffold(
        appBar: AppBar(
          title: Text(widget.title == null ? '新增2' : widget.title),
          actions: <Widget>[
            new Builder(builder: (BuildContext context) {
              return FlatButton(
                child: Text(
                  '保存',
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                onPressed: () {
                  _showError(context, _titleValue, _textValue);
                },
              );
            })
          ],
        ),
        body: new Builder(builder: (BuildContext context) {
          return Container(
            color: Global.defaultBgColor,
            padding: EdgeInsets.only(left: 0, right: 0),
            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                textFieldWidget(context),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Global.defaultTextBgColor,
                  child: Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: FlatButton(
                            padding: EdgeInsets.only(left: 0),
                            child: Image.asset(iconPath),
                            onPressed: () {
                              print('object');
                              showDate(context);
                            },
                          )),
                      Container(
//                      width: 200,
                        child: Text(clockStr),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        }));
    return _scaffold;
  }

  String timeStr() {
    setState(() {
      if (_clock == null) {
        clockStr = ' ';
      } else {
        clockStr = _clock.toString().substring(0, 19);
      }
    });
  }
}
