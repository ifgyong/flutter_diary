import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//GlobalKey
enum EasyHubType {
  EasyHub_msg, //只有文字
  EasyHub_just, //只有hub
  EasyHub_all, //都有
}

class EasyHub {
  static EasyHub _easyHub;

  OverlayEntry _entry;
  OverlayState _overlayState;
  List<OverlayEntry> _listAdd = [];
  List<Widget> _children = [];
  String msg = 'loading';
  TextStyle textStyle; //自定义
  bool needupdate = false; //下次需要更新
  EasyHubType _easyHubType;

  double textHeight;
  double height;
  double width;
  Color background;
  Color circlebackgroundColor; //动画 前景色
  Animation<Color> circleValueColor; //动画背景色

  EasyHub(
      {this.circleValueColor,
      this.circlebackgroundColor,
      this.background}); // => _getInstance();
  static EasyHub get getInstance {
    return _getInstance();
  }

  static EasyHub _getInstance(
      {Color background,
      Color circlebackgroundColor,
      Animation<Color> circleValueColor}) {
    if (_easyHub == null) {
      _easyHub = new EasyHub(
          background: background,
          circlebackgroundColor: circlebackgroundColor,
          circleValueColor: circleValueColor);
    }
    return _easyHub;
  }

  EasyHub setBackgroundColor(Color color) {
    if (color != null) this.background = color;
    return this;
  }

  EasyHub setCircleBackgroundColor(Color color) {
    if (color != null) this.circlebackgroundColor = color;
    return this;
  }

  EasyHub setValueColor(Animation<Color> color) {
    if (Color != null) this.circleValueColor = color;
    return this;
  }

  EasyHub setParameter(
      {Color background,
      Color circlebackgroundColor,
      Animation<Color> circleValueColor}) {
    this
      ..setValueColor(circleValueColor)
      ..setCircleBackgroundColor(circlebackgroundColor)
      ..setBackgroundColor(background);
  }

/*初始化*/
  EasyHub._initernal() {
//    _easyHub = EasyHub();
//    return _easyHub;
  }
/*对外公布接口 消失*/
  static void dismiss() {
    EasyHub.getInstance._dismiss();
  }

  void dismiss_hub() {
    this._dismiss();
  }

  void _dismiss() {
    if (_easyHub._listAdd.contains(_entry) && _entry != null) {
      _entry?.remove();
      _easyHub._listAdd.remove(_entry);
    }
  }

  static void dismiddAll() {
    for (int i = 0; i < EasyHub.getInstance._listAdd.length; i++) {
      EasyHub.getInstance._listAdd[i].remove();
    }
    EasyHub.getInstance._listAdd.clear();
  }

/*对外公布接口 展示 纯文字*/
  static void show(BuildContext context, String msg) {
    EasyHub.getInstance.needupdate = true;
    EasyHub.getInstance._easyHubType = EasyHubType.EasyHub_all;
    EasyHub.getInstance._show(msg, context);
  }

/*对外公布接口 展示 加载动画*/
  static void showHub(BuildContext context) {
    EasyHub.getInstance._showHub(context);
  }

  /*对外公布接口 展示 加载文本*/
  static void showMsg(
    BuildContext context,
    String msg,
  ) {
    assert(context != null, 'easy Hub context is null');
    EasyHub.getInstance._showMsg(msg, context);
  }

/*
* 实例方法 展示
* */
  void show_hub(
      {String msg,
      @required BuildContext context,
      @required EasyHubType type}) {
    this._easyHubType = type;
    this.needupdate = true;

    switch (type) {
      case EasyHubType.EasyHub_all:
        assert(msg != null, 'msg is null');
        this._show(msg, context);
        break;
      case EasyHubType.EasyHub_just:
        this._showHub(context);
        break;
      case EasyHubType.EasyHub_msg:
        assert(msg != null, 'msg is null');
        this._showMsg(msg, context);
        break;
    }
  }

  void _showHub(BuildContext context) {
    this._easyHubType = EasyHubType.EasyHub_just;
    this.needupdate = true;
    _show('', context);
  }

  void _showMsg(String msg, BuildContext context) {
    assert(context != null, 'easy Hub context is null');
    EasyHub.getInstance._easyHubType = EasyHubType.EasyHub_msg;
    EasyHub.getInstance.needupdate = true;
    _show(msg, context);
  }

  void _show(String msg, BuildContext context) {
    if (_overlayState == null) {
      _overlayState = Overlay.of(context);
    }
    if (this._easyHubType == EasyHubType.EasyHub_msg ||
        this._easyHubType == EasyHubType.EasyHub_all) {
      this.msg = msg;
    }

    //是否需要更新
    if (needupdate == true) {
//      _entry?.remove();
      this.updateView(context);
      _entry = null;
      needupdate = false;
    }

    if (_entry == null) {
      _entry = getLayer(context);
    }

    if (_easyHub._listAdd.contains(_entry) == false) {
      _overlayState.insert(_entry);
      _easyHub._listAdd.add(_entry);
    } else {
      _entry.remove();
      _easyHub._listAdd.remove(_entry);
      _overlayState.insert(_entry);
      _easyHub._listAdd.add(_entry);
    }
  }

  void updateView(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    if (_children == null) {
      _children = List();
    }
    _children.clear();
    if (this.msg == null) {
      this.msg = 'text is null';
    }

    Text _text = Text(
      this.msg.length > 0 ? this.msg : '',
      style: textStyle == null
          ? TextStyle(
              fontSize: 15,
              color: Colors.white,
              decoration: TextDecoration.none)
          : textStyle,
    );
    Widget _indicator = CircularProgressIndicator(
      backgroundColor: circlebackgroundColor,
      valueColor: circleValueColor,
    );
    double cirWidth = 80; //动画大小半径
    textHeight = this.msg.length > 0 ? 30 : 0;
    height = textHeight;
    width = _size.width - 40;
    Color defaultbgColor = Colors.black38;
    if (this.background != null) defaultbgColor = this.background;
    switch (_easyHubType) {
      case EasyHubType.EasyHub_msg:
        _children.add(Container(
          height: textHeight + 30,
          alignment: Alignment.center,
          color: defaultbgColor,
          child: _text,
        ));
        height += 30;
        break;
      case EasyHubType.EasyHub_all:
        _children.add(Container(
            height: cirWidth,
            alignment: Alignment.center,
            color: defaultbgColor,
            child: _indicator));
        _children.add(Container(
          height: textHeight,
          alignment: Alignment.center,
          color: defaultbgColor,
          child: _text,
        ));
        height += cirWidth;
        break;
      case EasyHubType.EasyHub_just:
        _children.add(Container(
            height: cirWidth,
            alignment: Alignment.center,
            color: defaultbgColor,
            child: _indicator));
        height = cirWidth;
        width = cirWidth;
        break;
    }
  }

  OverlayEntry getLayer(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    return new OverlayEntry(
//        maintainState: true,
        builder: (BuildContext con) {
      return Positioned(
        top: _size.height / 2 - height / 2,
        width: width,
        left: _size.width / 2 - width / 2,
        height: height,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5.0),
          child: Column(
            children: _children,
          ),
        ),
      );
    });
  }
}
