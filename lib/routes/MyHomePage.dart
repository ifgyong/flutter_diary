import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterbook/comment/Glo.dart';
import 'package:flutterbook/comment/config/LocalConfig.dart';
import 'package:flutterbook/routes/easy_hub.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutterbook/main.dart';
import 'AddArticle.dart';
import 'HistoryPage.dart';
import 'SecondPage.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.isToday}) : super(key: key);

  final String title;
  final bool isToday;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  List _data;
  int itemCount = 0;
  int _page = 1;
  JPush _jPush = new JPush();
  EasyRefreshController _controller;
  ScrollController _scrollController;
  @override
  Future<void> initState() {
    super.initState();
    update(1); //刷新列表
    initJpush();
    _controller = EasyRefreshController();
    _scrollController = ScrollController();
  }

  initJpush() {
    _jPush.setup(
        appKey: '3f0a16498e0d43b8fd9cd9f1',
        production: false,
        debug: true,
        channel: 'channel');
    _jPush.applyPushAuthority();

    _jPush.addEventHandler(onOpenNotification: (Map<String, dynamic> message) {
      print('open:' + message.toString());
      var ids = message['extras']['id'];
      navigatorKey.currentState.push(new MaterialPageRoute(
          builder: (context) => AddArticle(
                id: ids.toString(),
                title: '编辑',
                textValue: '',
                titleValue: '',
              )));
      return;
    }, onReceiveMessage: (Map<String, dynamic> message) {
      print('onReceiveMessage:' + message.toString());
      return;
    }, onReceiveNotification: (Map<String, dynamic> message) {
      print('onReceiveNotification:' + message.toString());
      return;
    });
    _jPush.setAlias('test').then((value) {
      print('alias : $value');
    });
    _jPush.getRegistrationID().then((id) {
      print('registid:$id');
    });
    _jPush.isNotificationEnabled().then((value) {
      print('推送可用性:$value');
    });
  }

  int getItemCount(List list) {
    if (list == null) {
      return 0;
    }
    return list.length;
  }

  void update(int pp) async {
    try {
      String url = Config.url() + '/api/list?page=$pp';
      if (widget.isToday != null && widget.isToday) {
        url = Config.url() + '/api/today?page=$pp';
      }
      EasyHub.getInstance
//        ..setBackgroundColor(Colors.black38)
//        ..setCircleBackgroundColor(Colors.lightGreen)
//        ..setValueColor(new AlwaysStoppedAnimation(Colors.black38))
        ..setParameter(
            background: Colors.black38,
            circleValueColor: new AlwaysStoppedAnimation(Colors.black38),
            circlebackgroundColor: Colors.lightGreen);
      ;
      EasyHub hub = EasyHub(
          circlebackgroundColor: Colors.black38,
          circleValueColor: new AlwaysStoppedAnimation(Colors.teal),
          background: Colors.black38);
      hub.show_hub(
          context: context, type: EasyHubType.EasyHub_msg, msg: 'loading');
      hub.dismiss_hub();

      EasyHub.show(context, 'loading');
      Response response = await Dio().get(url);
      EasyHub.dismiddAll();
      print(response.data.toString());
      setState(() {
        if (pp > 1) {
          _data += response.data['data']['data'];
        } else {
          _data = response.data['data']['data'];
        }
        itemCount = _data.length;
      });
      EasyHub.dismiss();
      setState(() {
        _page += 1;
      });

      _controller.finishRefresh(success: true, noMore: false);
      _controller.finishLoad(success: true, noMore: false);

//      print('异步回调：page:$pp,$_page' + _data.toString());
    } catch (e) {
      print('错误：' + e);
    }
  }

//列表
  ListView getListView() {
    return ListView.builder(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => AddArticle(
                        title: '添加提醒',
                        titleValue: _data[index]['title'],
                        textValue: _data[index]['content'],
                        id: _data[index]['id'].toString(),
                      )));
            },
            child: Container(
              margin: EdgeInsets.only(top: 5, bottom: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Container(
                  color: Global.defaultTextBgColor,
                  width: 100,
                  height: 103,
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 15, right: 15, top: 8, bottom: 8),
                    child: Column(
//                      padding: EdgeInsets.only(left: 15, right: 15),

                      children: <Widget>[
                        Flex(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            Expanded(
                              flex: 5,
//                              alignment: Alignment.topLeft,
//                              padding: EdgeInsets.only(
//                                  left: 15, right: 15, top: 8, bottom: 8),
                              child: Text(
                                _data[index]['title'],
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              ),
                            ),
                            Text(
                              _data[index]['time'] == null
                                  ? ''
                                  : _data[index]['time'],
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black38),
                            )
                          ],
                        ),
                        Container(
//                          padding: EdgeInsets.only(left: 15, right: 15),
                          alignment: Alignment.topLeft,
                          child: Text(
                            _data[index]['content'],
                            style:
                                TextStyle(fontSize: 13, color: Colors.black38),
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
      itemCount: itemCount,
    );
  }

  void popDrawer(int index, BuildContext context) {
    Navigator.pop(context);
    List titles = ['今天', '历史', '关心的'];

    if (index <= 1) {
      Navigator.of(context).push(new MaterialPageRoute(
          builder: (context) => HistoryPage(
                isToday: index == 0,
                title: titles[index],
              )));
    } else {
      Navigator.of(context).push(new MaterialPageRoute(
          builder: (context) => SecondPage(
                title: titles[index],
                content: '我是描述' + titles[index],
              )));
    }
  }

// 获取后台view
  Drawer getDrawer(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        Container(
          height: 70,
          width: 100,
          child: Image.asset('images/header.png'),
        ),
        Container(
          child: Text('兜兜转转'),
          width: 160,
          margin: EdgeInsets.only(left: 120),
        ),
        ListTile(
          leading: Image.asset('images/today.png'),
          title: Text('今天'),
          onTap: () {
            popDrawer(0, context);
          },
        ),
        ListTile(
          leading: Image.asset('images/lishi.png'),
          title: Text('历史'),
          onTap: () {
            popDrawer(1, context);
          },
        ),
        ListTile(
          leading: Image.asset('images/heart.png'),
          title: Text('我关心的'),
          onTap: () {
            popDrawer(2, context);
          },
        ),
      ],
    ));
  }

  List tabs = ['首页', '广场', '我'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.list),
//            onPressed: () {
//              print('object');
//            },
//          )
//        ],
      ),
      drawer: getDrawer(context),
      body: Container(
        color: Global.defaultBgColor,
        alignment: Alignment.topLeft,
        child: EasyRefresh(
          child: getListView(),
          onLoad: () async {
            print('onload');
            update(_page);
          },
          onRefresh: () async {
            print('onRefresh');
            _page = 1;
            update(_page);
          },
          controller: _controller,
          scrollController: _scrollController,
          enableControlFinishLoad: true,
          enableControlFinishRefresh: true,
        ),
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {
////          _controller.finishRefresh(success: false);
////          _controller.finishLoad(success: false);
////          _controller.resetLoadState();
////          _controller.resetRefreshState();
//          Navigator.pushNamed(context, 'add', arguments: {});
//        },
//        tooltip: '',
//        child: Icon(Icons.add),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
