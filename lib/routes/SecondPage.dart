import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterbook/routes/easy_hub.dart';

class SecondPage extends StatefulWidget {
  String title;
  String content;
  SecondPage({Key key, this.title, this.content}) : super(key: key);

  _SecondPage createState() => _SecondPage();
}

GlobalKey<OverlayState> _key = GlobalKey<OverlayState>();

class _SecondPage extends State<SecondPage> {
  OverlayEntry _entry;
  ListView getListView() {
    return ListView.builder(
      itemBuilder: (BuildContext con, int index) {
        return Container(
//          color: Colors.lightBlueAccent,
          alignment: index % 3 == 0
              ? Alignment.centerLeft
              : index % 3 == 1 ? Alignment.center : Alignment.centerRight,
          child: Text('$index' +
              (widget.content != null ? widget.content : widget.title)),
        );
      },
    );
  }

  var valueSlider = 0.7;
  /*进度条view*/
  Slider getSlider() {
    return Slider(
      value: valueSlider,
      activeColor: Colors.redAccent,
      onChanged: (value) {
        print(value);
        setState(() {
          valueSlider = value;
        });
      },
    );
  }

/*底部按钮*/
  FlatButton button(String title, int tag) {
    return FlatButton(
      child: Text(title),
      onPressed: () {
        print('$tag is click');
//        _entry.remove();
//        Navigator.pop(context);
        _entry?.remove();
      },
    );
  }

/*toast view */
  String _nodataString = '123';
  OverlayEntry getLayer(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    Text _text = Text(
      '用户姓名不对，请重新输入',
      style: TextStyle(
          fontSize: 15, color: Colors.white, decoration: TextDecoration.none),
    );
    return OverlayEntry(
        maintainState: true,
        builder: (BuildContext con) {
          return Positioned(
            top: _size.height / 2 - 30,
            width: _size.width - 40,
            left: 20,
            height: 60,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                  alignment: Alignment.center,
                  color: Colors.black38,
                  child: LinearProgressIndicator()),
            ),
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  int index = 0;
  @override
  Widget build(BuildContext context) {
    _entry = getLayer(context);
    Size _size = MediaQuery.of(context).size;
    double top = MediaQueryData.fromWindow(window).padding.top + 44;
    EdgeInsets viewpadding = MediaQueryData.fromWindow(window).viewPadding;
    print('$_size ，viewpad:$viewpadding');
    return Scaffold(
//        persistentFooterButtons: <Widget>[
//          IconButton(icon: Icon(Icons.add), onPressed: null),
//          IconButton(icon: Icon(Icons.my_location), onPressed: null),
//          IconButton(icon: Icon(Icons.people), onPressed: null),
//        ],
        bottomSheet: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              color: Colors.redAccent,
              onPressed: () {
//                Overlay.of(context).insert(_entry);
                int cc = index % 3;
                print(cc);
                switch (cc) {
                  case 0:
                    EasyHub.show(context, 'loading');
                    break;
                  case 1:
                    EasyHub.showHub(context);
                    break;
                  case 2:
                    EasyHub.showMsg(context, '加载文字展示');
                    break;
                }
//                EasyHub.dismiddAll();
                index += 1;
                Future.sync(() {}).then((v) {
                  Future.delayed(Duration(seconds: 2)).then((v) {
//                    _entry?.remove();
//                    EasyHub.print('$_entry removed');
                    EasyHub.dismiddAll();
                  });
                });
              },
              child: Text('页面底部按钮 - '),
            ),
          ],
        ),
//        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Stack(
            children: <Widget>[
              getListView(),
              Positioned(
                top: () {
                  return (_size.height - top - 44 /*底部按钮的高度*/) / 2 - 15;
                }(), //top,
                left: _size.width / 2 - 60,
                width: 0,
                height: 0,
//                bottom: 0.0,
                child: Container(
                  color: Colors.lightGreenAccent,
                  alignment: Alignment.center,
                  child: Text(_nodataString),
                ),
              )
            ],
          ),
        ));
  }
}
