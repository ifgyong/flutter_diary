import 'package:flutter/material.dart';
import 'package:flutterbook/main.dart';
import 'package:flutterbook/routes/MePage.dart';
import 'package:flutterbook/routes/SecondPage.dart';
import 'AddArticle.dart';
import 'MyHomePage.dart';

class TabbraDIY extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TabbatDIY();
  }
}

class _TabbatDIY extends State<TabbraDIY> {
  int _selectIndex = 0;
  List<Widget> _pages = [
    MyHomePage(
      title: '首页',
    ),
//    AddArticle(
//      title: '添加提醒',
//    ),
//    MePage(),
    SecondPage(
      title: '第二页',
      content: '我是第二页的具体 描述',
    ),
  ];
  BottomNavigationBar getBottomNavigationBar() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
            title: Text('首页'),
            icon: Icon(Icons.pages),
            activeIcon: Icon(Icons.add)),
        BottomNavigationBarItem(
            title: Text('我'),
            icon: Icon(Icons.accessible),
            activeIcon: Icon(Icons.accessibility_new))
      ],
      type: BottomNavigationBarType.fixed,
      onTap: _clickIndex,
      currentIndex: _selectIndex,
    );
  }

  BottomAppBar getBottomAppBar() {
    return BottomAppBar(
//      color: Colors.redAccent,
      shape: CircularNotchedRectangle(),
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.account_box),
            onPressed: () {
              _clickIndex(0);
            },
          ),
          SizedBox(),
          IconButton(
            icon: Icon(Icons.accessible),
            onPressed: () {
              _clickIndex(1);
            },
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_selectIndex],
      bottomNavigationBar: getBottomAppBar(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print('🐭会打洞');
          Navigator.pushNamed(context, 'add', arguments: {});
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  void _clickIndex(int index) {
    setState(() {
      _selectIndex = index;
    });
  }
}
