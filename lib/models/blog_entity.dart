import 'package:flutterbook/generated/json/base/json_convert_content.dart';
import 'package:data_plugin/bmob/table/bmob_object.dart';

class BlogEntity extends BmobObject with JsonConvert<BlogEntity> {
  String title;
  String content;
  BlogEntity();
  @override
  Map getParams() {
    // TODO: implement getParams

    return {"title": this.title, "content": this.content};
  }
}
