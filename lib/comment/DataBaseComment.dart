import 'package:data_plugin/bmob/response/bmob_saved.dart';

import 'package:flutter/widgets.dart';
import 'package:data_plugin/bmob/table/bmob_user.dart';
import 'package:flutterbook/comment/category.dart';
import 'package:flutterbook/generated/json/blog_entity_helper.dart';
import 'package:flutterbook/models/blog_entity.dart';

class DataBaseComment {
  static saveSingle() {
    String currentObjectId;
//    BmobUser bmobUser = BmobUser();
//    bmobUser.objectId = "7c7fd3afe1";
    BlogEntity blog = BlogEntity();
    blog.title = "博客标题";
    blog.content = "博客内容";
    blog.objectId = StringToMD5(DateTime.now().toString());
    blog.save().then((BmobSaved bmobSaved) {
      String message =
          "创建一条数据成功：${bmobSaved.objectId} - ${bmobSaved.createdAt}";
      currentObjectId = bmobSaved.objectId;
//      showSuccess(context, message);
      print(message);
      print(blogEntityToJson(blog));
    }).catchError((e) {
      print('错误：$e');
//      showError(context, BmobError.convert(e).error);
    });
  }
}
