enum Env {
  DEBUG,
  Release,
}

class Config {
//  static LcalConfig aa = '';
  static Env _env =
      bool.fromEnvironment("dart.vm.product") == true ? Env.Release : Env.DEBUG;

  static String url() {
    switch (_env) {
      case Env.DEBUG:
        return 'http://10.11.10.218:81';
      case Env.Release:
        return 'http://10.11.10.218:80';
    }
  }
}
