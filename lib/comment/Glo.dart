import 'dart:ui';

import 'package:flutter/material.dart';

class Global {
  static Color defaultBgColor = Color.fromRGBO(239, 238, 248, 1.0);
  static Color defaultTextBgColor = Color.fromRGBO(254, 254, 254, 1);

  static final String keysTitles = 'keysTitles';
  static final String keysContents = 'keysContents';
}
