import 'dart:io';

import 'package:data_plugin/bmob/bmob.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterbook/comment/DataBaseComment.dart';
import 'package:flutterbook/comment/Glo.dart';
import 'package:flutterbook/comment/config/LocalConfig.dart';
import 'package:flutterbook/routes/AddArticle.dart';
import 'package:flutterbook/routes/SecondPage.dart';
import 'package:flutterbook/routes/TabbarDIY.dart';

void main() {
  Global();
  runApp(MyApp());
}

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new TabbraDIY(),
      routes: {
        'add': (context) => AddArticle(
              title: '添加提醒',
            ),
        'new': (context) => SecondPage(
              title: '我是第二页的描述',
            ),
      },
    );
  }
}
