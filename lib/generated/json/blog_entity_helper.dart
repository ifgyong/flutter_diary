import 'package:flutterbook/models/blog_entity.dart';

blogEntityFromJson(BlogEntity data, Map<String, dynamic> json) {
  if (json['title'] != null) {
    data.title = json['title']?.toString();
  }
  if (json['content'] != null) {
    data.content = json['content']?.toString();
  }
  return data;
}

Map<dynamic, dynamic> blogEntityToJson(BlogEntity entity) {
  final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
  data['title'] = entity.title;
  data['content'] = entity.content;
  return data;
}
